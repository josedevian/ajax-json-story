from django.shortcuts import render
from django.http import JsonResponse
from django.conf import settings
from .models import FavedBook

# Create your views here.
response = {}

def index(request):
    favedBook = FavedBook.objects.all()
    count = FavedBook.objects.count()
    favedBookID = [book.bookID for bookID in favedBook]

    books_json = settings.BOOK_LIST
    response['books'] = books_json['items']
    response['faved'] = favedBookID
    response['count'] = count

    return render(request, 'index.html', response)

def add(request):
    if request.method == 'POST':
        bookID = request.POST['bookID']
        FavedBook.objects.create(bookID=bookID)
        count = FavedBook.objects.count()
        return JsonResponse({'count':count})

def remove(request):
    if request.method == 'POST':
        bookID = request.POST['bookID']
        FavedBook.objects.get(bookID=bookID).delete()
        count = FavedBook.objects.count()
        return JsonResponse({'count':count})
