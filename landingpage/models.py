from django.db import models

# Create your models here.

class FavedBook(models.Model):
    bookID = models.CharField(max_length=300, unique=True, primary_key=True)
