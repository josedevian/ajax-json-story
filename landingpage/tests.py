from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import index, add, remove
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class LandingPageLabUnitTest(TestCase):

    def test_landingpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_userprofile_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class LandingPageFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def test_body_background_with_css_property(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        body_style = selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
        time.sleep(3)
        self.assertIn('Helvetica Neue', body_style)

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()
