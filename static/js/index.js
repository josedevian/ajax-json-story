$(function () {
    $(window).on('load', function () {
      $('#status').delay(800).fadeOut('slow');
    })
});

var favedIcon = "{% static 'images/favorited.png' %}"
var defaultIcon = "{% static 'images/default.png' %}"

function addToFavorites(caller, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/add/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {book_id: id},
        success: function (result) {
            $(caller).attr('src', favoritedIcon);
            $("#counter").replaceWith('<span id="counter">' + result.count + '</span>');
            loadAgain();
            console.log("Success add to bookmark!");
        },
        error: function () {
            alert("Error, cannot get data from server!")
        },
    });
}

function removeFromFavorites(caller, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/remove/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {book_id: id},
        success: function (result) {
            $(caller).attr('src', defaultIcon);
            $("#counter").replaceWith('<span id="counter">' + result.count + '</span>');
            loadAgain();
            console.log("Success remove from bookmark!");
        },
        error: function () {
            alert("Error, cannot get data from server!")
        }
    })
}

function loadAgain() {
    $.get("/",
        function (data) {
            var pageUpdate = jQuery(data);
            var favoritedUpdate = pageUpdate.find("#table").html();
            $("#table").html(favoritedUpdate);
        });
}

$(document).ready(function () {
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });
});

function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    sendToken(id_token);
    console.log("Logged In");
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function signIn() {
  location.href = "{% url 'login'}";
}

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function (result) {
            console.log("Successfully send token");
            if (result.status === "0") {
                html = "<h4 class='success'>Logged In</h4>";
                window.location.replace(result.url);
            } else {
                html = "<h4 class='fail'>Something error, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}
