from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import login, logout
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class LoginPageLabUnitTest(TestCase):
    def test_loginpage_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_loginpage_using_index_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

class LoginPageFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LoginPageFunctionalTest, self).setUp()

    def test_body_background_with_css_property(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        body_style = selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
        time.sleep(3)
        self.assertIn('Helvetica Neue', body_style)

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(LoginPageFunctionalTest, self).tearDown()
