$(function () {
    $(window).on('load', function () {
      $('#status').delay(800).fadeOut('slow');
    })
});

var favedIcon = "{% static 'images/favorited.png' %}"
var defaultIcon = "{% static 'images/default.png' %}"

function addToFavorites(caller, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/add/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {book_id: id},
        success: function (result) {
            $(caller).attr('src', favoritedIcon);
            $("#counter").replaceWith('<span id="counter">' + result.count + '</span>');
            loadAgain();
            console.log("Success add to bookmark!");
        },
        error: function () {
            alert("Error, cannot get data from server!")
        },
    });
}

function removeFromFavorites(caller, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/remove/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {book_id: id},
        success: function (result) {
            $(caller).attr('src', defaultIcon);
            $("#counter").replaceWith('<span id="counter">' + result.count + '</span>');
            loadAgain();
            console.log("Success remove from bookmark!");
        },
        error: function () {
            alert("Error, cannot get data from server!")
        }
    })
}

function loadAgain() {
    $.get("/",
        function (data) {
            var pageUpdate = jQuery(data);
            var favoritedUpdate = pageUpdate.find("#table").html();
            $("#table").html(favoritedUpdate);
        });
}
